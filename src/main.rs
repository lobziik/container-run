use anyhow::{bail, Result};
use std::env;
use std::os::unix::process::CommandExt;
use std::process::Command;

#[macro_use]
extern crate log;

mod configuration;

const PODMAN: &str = "podman";

fn main() -> Result<()> {
    let loglevel = get_log_level();
    simple_log::quick!(loglevel);

    // target args are the command line argument passed to the container
    let mut target_args: Vec<String> = Vec::new();
    for a in env::args().skip(1) {
        target_args.push(a);
    }

    if target_args.is_empty() {
        bail!("no commands passed to target container");
    }

    if target_args[0] == "--help" {
        usage();
        return Ok(());
    }

    if target_args[0].starts_with("--") {
        bail!(format!(
            "first argument must be a command, looks like a flag: {}",
            target_args[0]
        ));
    }

    let path = env::current_dir().expect("cannot determine current dir");
    let basename = &path
        .file_name()
        .expect("cannot separate base path")
        .to_str()
        .unwrap();
    debug!("detected basename {:?}", &basename);

    let config = configuration::build_configuration(basename);
    debug!("config = {:?}", config);

    let engine = PODMAN;

    // selinux labeling does not work on macos yet, changing volume mount argument there for now
    let volume_mount_parameter = match cfg!(target_os = "macos") && engine == PODMAN {
        true => {
            warn!(
                "MacOS with podman detected, selinux labeling for volumes disabled. \
                See https://github.com/containers/podman/issues/13631#issuecomment-1077643246 \
                for more info"
            );
            format!("--volume={}:{}", path.to_str().unwrap(), &config.mountpoint)
        }
        false => format!(
            "--volume={}:{}:Z",
            path.to_str().unwrap(),
            &config.mountpoint
        ),
    };
    // container runtime args are the full list of arguments pass to the container runtime engine
    let mut container_engine_args = vec![
        "run".to_string(),
        "--rm".to_string(),
        "-it".to_string(),
        volume_mount_parameter,
        format!("--workdir={}", config.mountpoint),
    ];

    // assemble the full arguments to the exec
    let mut full_args: Vec<String> = Vec::new();
    full_args.append(&mut container_engine_args);
    full_args.push(config.image);
    full_args.append(&mut target_args);

    // exec the container command and hand over execution
    debug!(
        "assembled exec command \"{} {}\"",
        &engine,
        full_args.join(" ")
    );
    Command::new(engine).args(full_args).exec();
    Ok(())
}

fn get_log_level() -> String {
    let verbosity_key = "CONTAINER_RUN_VERBOSITY";
    let warn = String::from("warn");
    match env::var(verbosity_key) {
        Ok(val) => match val.as_str() {
            "debug" => String::from("debug"),
            _ => warn,
        },
        _ => warn,
    }
}

fn usage() {
    println!("usage: container-run command_to_run_in_container arg1 arg2 --flag1 arg3");
}
