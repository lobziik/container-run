use serde_json::Value;
use std::env;
use std::fs::File;
use std::io::BufReader;

#[derive(Debug)]
pub(crate) struct Configuration {
    pub image: String,
    pub mountpoint: String,
}

pub(crate) fn build_configuration(basename: &str) -> Configuration {
    let filename = get_config_filename();
    let file = File::open(filename).expect("unable to open configuration file");
    let reader = BufReader::new(file);
    let v: Value =
        serde_json::from_reader(reader).expect("unable to deserialize configuration file");

    let profile = profile_for_basename(&v, basename);
    debug!("profile = {:?}", &profile);
    let image = image_for_profile(&v, &profile);
    debug!("image = {:?}", &image);
    let mountpoint = mountpoint_for_profile(&v, basename, &profile);
    debug!("mountpoint = {:?}", &mountpoint);

    Configuration { image, mountpoint }
}

fn get_config_filename() -> String {
    let home_key = "HOME";
    let home = env::var(home_key).expect("no HOME directory found");

    let possible_file_paths = [
        String::from("./.container-run.conf"),
        [home.as_str(), ".container-run.conf"].join("/"),
        [home.as_str(), ".config/container-run/container-run.conf"].join("/"),
    ];

    for p in possible_file_paths {
        let f = File::open(&p);
        match f {
            Ok(_) => {
                debug!("found configuration file {:?}", p);
                return p;
            }
            _ => debug!("configuration file {:?}, not found", p),
        }
    }

    panic!("no configuration file found");
}

fn profile_for_basename(value: &Value, basename: &str) -> String {
    let profile = &value["basenames"][basename]["profile"];
    match profile.is_string() {
        true => String::from(profile.as_str().unwrap()),
        _ => String::from("defaults"),
    }
}

fn image_for_profile(value: &Value, profile: &str) -> String {
    let default_image = &value["defaults"]["image"];
    let image = &value["profiles"][profile]["image"];
    match image.is_string() {
        true => String::from(image.as_str().unwrap()),
        _ => match default_image.is_string() {
            true => String::from(default_image.as_str().unwrap()),
            _ => panic!("no image found for profile {:?}, or default", profile),
        },
    }
}

fn mountpoint_for_profile(value: &Value, basename: &str, profile: &str) -> String {
    let default_mountpoint = &value["defaults"]["mountpoint"];
    let mountpoint = &value["profiles"][profile]["mountpoint"];
    match mountpoint.is_string() {
        true => String::from(mountpoint.as_str().unwrap()).replace("{basename}", basename),
        _ => match default_mountpoint.is_string() {
            true => {
                String::from(default_mountpoint.as_str().unwrap()).replace("{basename}", basename)
            }
            _ => panic!("no mountpoint found for profile {:?}, or default", profile),
        },
    }
}
