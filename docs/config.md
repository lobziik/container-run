# container-run.conf

The configuration file for container-run contains mappings of directory
basenames to run environment profiles. These profiles dictate the container
image to use and working directory mount path.

## Location

container-run will look in three places to find a configuration file, in
order of precedence they are:
* `./.container-run.conf`
* `~/.container-run.conf`
* `~/.config/container-run/container-run.conf`

## Format

The configuration file is [JSON](https://www.json.org/json-en.html) formatted.
The following example demonstrates the schema:

```json
{
    "basenames": {
        "myproject": {"profile": "golang-github"}
    },
    "defaults": {
        "image": "docker.io/library/bash",
        "mountpoint": "/src/{basename}"
    },
    "profiles": {
        "golang-github": {
            "image": "docker.io/library/golang:1.16",
            "mountpoint": "/go/src/github.com/{basename}"
        }
    }
}
```

In this example, when running `container-run` in any directory named `myproject`
the image used will be `docker.io/library/golang:1.16` and the working
directory will be mounted at `/go/src/github.com/myproject`. Note that
container-run will substitute the basename for `{basename}` in `mountpoint`
fields.

The `defaults` field can be used to define a default image and mount point to
be used when the working directory base name is not explicitly configured.
